package com.group1.blindmovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlindMovieApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlindMovieApplication.class, args);
	}

}
